package ru.inno.lec06;

import ru.inno.lec06.entity.Human;
import ru.inno.lec06.entity.HumanAnnotation;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Arrays;

public class ReflectionDemo {
    public static void main(String[] args) throws IllegalAccessException {
        Class<Human> aClass = Human.class;
        Human human = new Human();
        System.out.println(human);

        System.out.println(aClass.getName());
        System.out.println(aClass.getSimpleName());
        System.out.println(aClass.getCanonicalName());

        Field[] fields = aClass.getDeclaredFields();
        for (Field declaredField : fields) {
            System.out.println(declaredField.getName()
                + " " + Modifier.isFinal(declaredField.getModifiers()));
            declaredField.setAccessible(true);
        }


        // чтение private-поля
        String name = (String) fields[1].get(human);
        System.out.println(name);

        // изменение
        fields[1].set(human, "Коля");
        fields[0].set(human, "Пришелец");
        fields[2].setInt(human, 500);

        System.out.println(human);

        // работа с аннотациями
        Annotation[] annotations = aClass.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println("Аннотация " + annotation);
        }
        HumanAnnotation annotation = aClass.getAnnotation(HumanAnnotation.class);
        if (annotation != null) {
            System.out.println(annotation.name());
        }
    }
}
