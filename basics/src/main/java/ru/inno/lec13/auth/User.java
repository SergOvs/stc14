package ru.inno.lec13.auth;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static ru.inno.lec13.Main.LOGIN;

public class User {
    private static final Logger LOGGER
            = LoggerFactory.getLogger(User.class);

    public boolean doLogin(String login){
        LOGGER.info("попытка зайти под пользователем {}, {}, {}",
                login, login, login);

        if ("user".equals(login)){
            LOGGER.info("пользователь успешно авторизован");
            MDC.put(LOGIN, login);
            return true;
        }

        if ("root".equals(login)) {
            LOGGER.warn("Запрещено логиниться супер-пользователем: {}", login);
            throw new UnsupportedOperationException();
        }
        LOGGER.info("Пользователь не авторизован");
        return false;
    };
}
