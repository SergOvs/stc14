package ru.inno.lec07;

import java.io.File;
import java.nio.file.Files;

public class Main {
    public static void main(String[] args)
            throws ClassNotFoundException,
            IllegalAccessException,
            InstantiationException {
        System.out.println("Сейчас будет магия");
        Magic magic = new RudeMagic();
        magic.doMagic();

        ClassLoader cl = new MyClassLoader();
        Class<?> kindClass = cl.loadClass("ru.inno.lec07.KindMagic");
        Magic kindMagic = (Magic) kindClass.newInstance();
        kindMagic.doMagic();

        KindMagic km1 = new KindMagic();
        KindMagic km2 = (KindMagic) kindMagic;

        System.out.println("Всё, конец");
    }
}
