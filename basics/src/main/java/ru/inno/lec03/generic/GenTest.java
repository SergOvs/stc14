package ru.inno.lec03.generic;

import ru.inno.lec03.generic.entities.Cat;
import ru.inno.lec03.generic.entities.Pet;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GenTest {
    public static void main(String[] args) {

        Cat[] cats1 = {new Cat()};
        Pet[] pets2 = cats1;

        List<Cat> cats = getPets();
        List<Pet> pets = new ArrayList<>(cats);
        Collections.copy(pets, cats);
        callPets(pets);
    }

    // <? extends Pet>
    private static <T extends Pet, Closeable> T callPets(List<T> pets) {
        for (T pet : pets) {
            System.out.println(pet);
        }
        return pets.get(0);
    }

    private static List<Cat> getPets() {
        List<Cat> result = new ArrayList<>();
        result.add(new Cat());
        return result;
    }
}
