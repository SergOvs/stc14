package ru.inno.lec09.java8;

import java.util.Comparator;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) {
        InterfaceImpl impl = new InterfaceImpl();
        impl.doSome();

        AtomicInteger k = new AtomicInteger(3);
        compare(new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                System.out.println("anonymous class = " + this.getClass());
                return k.incrementAndGet();
            }
        });

        compare((Object obj1, Object obj2) -> {return 0;});
        compare((obj1, obj2) -> k.incrementAndGet());

        Comparator cp = (o1, o2) -> 0;
        System.out.println("lambda class = " + cp.getClass());
        compare(cp);

        // demo для PrinterInterface

        PrinterInterface printerInterface = impl::printHello;
        doPrint(printerInterface);
        doPrint(impl);
        //doPrint(impl::printBye);
    }

    private static void doPrint(PrinterInterface printer) {
        printer.print();
    }

    public static void compare(Comparator comparator){
        System.out.println(comparator.compare(new Object(), new Object()));
    }
}
