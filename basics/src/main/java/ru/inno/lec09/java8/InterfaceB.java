package ru.inno.lec09.java8;

public interface InterfaceB {
    default void doSome() {
        System.out.println("doSome");
    }
}
