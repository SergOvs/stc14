package ru.inno.lec09.java8;

public class InterfaceImpl
        implements InterfaceA, InterfaceB, PrinterInterface{

    private String value = "123";

    @Override
    public void doSome() {
        System.out.println("Impl");
    }

    public String printHello() {
        System.out.println("Hello! " + value);
        return "";
    }

    public void printBye() {
        System.out.println("bye");
    }

    @Override
    public String print() {
        System.out.println("print some");
        return "";
    }
}
