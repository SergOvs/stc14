package ru.inno.lec09.java8;

@java.lang.FunctionalInterface
public interface PrinterInterface {
    String print();
}
