package ru.inno.lec09.java8;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

public class StreamDemo {

    public static void main(String[] args) {
        Collection<Integer> ints = Arrays.asList(1, 2, 3, 4, 5, 6);

        int sum = 0;
        for (Integer integer : ints) {
            if (integer % 2 == 1) {
                sum += integer;
            }
        }

        System.out.println("sum=" + sum);

        // stream api

        int sum2 = ints.stream()
                .filter(o -> {
                    System.out.println("filter " + o);
                    return o % 2 == 1;
                })
                .reduce(0, (o1, o2) -> {
                    System.out.println("reduce " + o1 + ", " + o2);
                    return o1 + o2;
                });
        System.out.println("sum=" + sum2);
    }
}
