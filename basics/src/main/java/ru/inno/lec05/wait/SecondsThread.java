package ru.inno.lec05.wait;

import java.util.Scanner;

public class SecondsThread extends Thread {
    private final int seconds;

    public SecondsThread(int seconds) {
        this.seconds = seconds;
    }

    @Override
    public void run() {
        int j = 0;
        while (!isInterrupted()) {
            synchronized (ChronoThread.MONITOR) {
                try {
                    ChronoThread.MONITOR.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if ((++j % seconds) == 0) {
                System.out.println("Seconds " + seconds);
            }
        }

    }
}
