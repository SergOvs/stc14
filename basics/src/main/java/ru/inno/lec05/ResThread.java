package ru.inno.lec05;

public class ResThread extends Thread {
    private final Counter counter;

    public ResThread(Counter counter) {
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            counter.increment();
        }
    }
}
