package ru.inno.lec05;

public class Counter {
    private int count;

    public synchronized int getCount() {
        return count;
    }

    public synchronized void increment() {
        this.count++;
    }
}
