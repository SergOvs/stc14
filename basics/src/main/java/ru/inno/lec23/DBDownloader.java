package ru.inno.lec23;

import org.springframework.stereotype.Component;
import ru.inno.lec23.api.Downloader;

//@Component
public class DBDownloader implements Downloader {
    @Override
    public Object download(String path) {
        System.out.println("Загружаю данные \""
            + path + "\""
        );
        return "Some ConTeNT";
    }
}
