package ru.inno.lec23.api;

public interface Downloader {
    Object download(String path);
}
