package ru.inno.lec23.api;

public interface Uploader {

    boolean upload(String path, Object obj);
}
