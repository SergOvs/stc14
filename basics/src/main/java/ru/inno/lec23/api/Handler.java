package ru.inno.lec23.api;

public interface Handler {
    boolean process(String src, String dest);
}
