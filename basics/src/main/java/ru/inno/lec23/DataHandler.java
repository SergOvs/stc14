package ru.inno.lec23;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import ru.inno.lec23.api.Downloader;
import ru.inno.lec23.api.Handler;
import ru.inno.lec23.api.Uploader;

@Component
public class DataHandler implements Handler {

    private Downloader downloader;
    private Uploader uploader;

    @Autowired
    public DataHandler(Downloader downloader, Uploader uploader) {
        this.downloader = downloader;
        this.uploader = uploader;
    }

    public DataHandler() {
    }

    public void setDownloader(Downloader downloader) {
        this.downloader = downloader;
    }

    public void setUploader(Uploader uploader) {
        this.uploader = uploader;
    }

    @Override
    public boolean process(String src, String dest) {
        Object obj = downloader.download(src);
        transform(obj);
        return uploader.upload(dest, obj);
    }

    private void transform(Object obj) {
        System.out.println("Трансформируем объект " + obj);
    }
}
