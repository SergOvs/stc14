package ru.inno.lec23;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.inno.lec23.api.Handler;

@Configuration
@ComponentScan
public class Main {
    public static void main(String[] args) {
        ApplicationContext ctx =
                new AnnotationConfigApplicationContext(Main.class);
        //        new ClassPathXmlApplicationContext("mySimpleContext.xml");

        Handler handler = ctx.getBean("dataHandler", Handler.class);

        System.out.println("Результат: "
                + handler.process("Источник", "Получатель"));
    }
}
