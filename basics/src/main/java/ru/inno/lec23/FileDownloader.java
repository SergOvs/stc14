package ru.inno.lec23;

import org.springframework.stereotype.Component;
import ru.inno.lec23.api.Downloader;

@Component("downloader4")
public class FileDownloader implements Downloader {
    @Override
    public Object download(String path) {
        System.out.println("Загружаю файл из \""
            + path + "\""
        );
        return "Some ConTeNT";
    }
}
