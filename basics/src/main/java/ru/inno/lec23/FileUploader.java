package ru.inno.lec23;

import org.springframework.stereotype.Component;
import ru.inno.lec23.api.Uploader;

@Component
public class FileUploader implements Uploader {
    @Override
    public boolean upload(String path, Object obj) {
        System.out.println(
                new StringBuilder("Выгружаю в файл \"")
                .append(path)
                .append("\" объект ")
                .append(obj)
                .toString()
        );
        return true;
    }
}
