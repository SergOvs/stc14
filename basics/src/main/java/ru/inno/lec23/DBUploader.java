package ru.inno.lec23;

import ru.inno.lec23.api.Uploader;

public class DBUploader implements Uploader {
    @Override
    public boolean upload(String path, Object obj) {
        System.out.println(
                new StringBuilder("Выгружаю в бд \"")
                .append(path)
                .append("\" объект ")
                .append(obj)
                .toString()
        );
        return true;
    }
}
