package ru.inno.lec14;

/**
 * Замечательный класс для тестирования
 */
public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int divide(int a, int b) throws DivideByZeroException {
        if (b == 0) {
            throw new DivideByZeroException();
        }
        return a / b;
    }

    public double divide(double a, double b) throws DivideByZeroException {
        if (b == 0) {
            throw new DivideByZeroException();
        }
        return a / b;
    }
}
