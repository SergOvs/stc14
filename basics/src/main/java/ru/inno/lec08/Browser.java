package ru.inno.lec08;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class Browser {

    //public static final String ADDRESS = "https://ya.ru";
    //public static final String ADDRESS = "file:///C:\\Users\\serge\\IdeaProjects\\STC14\\src\\TextFile.txt";
    public static final String ADDRESS = "classpath://TextFile.txt";

    public static void main(String[] args) throws IOException {
        URL.setURLStreamHandlerFactory(new ClassPathUrlHandlerFactory());
        URL url = new URL(ADDRESS);
        try (BufferedReader is =
                     new BufferedReader(
                             new InputStreamReader(url.openStream()))) {
            String line;
            while ((line = is.readLine()) != null) {
                System.out.println(line);
            }
        }
    }
}
