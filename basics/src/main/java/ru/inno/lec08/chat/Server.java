package ru.inno.lec08.chat;

import java.io.*;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server {
    public static final int PORT = 3232;

    public static void main(String[] args) throws IOException, InterruptedException {
        ServerSocket serverSocket = new ServerSocket(PORT);
        System.out.println("start waiting on port " + PORT);
        Thread listener = new ListenerThread(serverSocket);
        listener.start();

        Scanner scanner = new Scanner(System.in);
        while (!"quit".equals(scanner.nextLine())) {
        }
        System.out.println("stopping server...");
        serverSocket.close();
        listener.interrupt();
        listener.join();
        System.out.println("server done");
    }
}
