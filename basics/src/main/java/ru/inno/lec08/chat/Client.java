package ru.inno.lec08.chat;

import java.io.*;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        try (Socket socket = new Socket("127.0.0.1", Server.PORT)) {
            OutputStream os = socket.getOutputStream();
            InputStream is = socket.getInputStream();
            os.write("Hello, world!\n".getBytes());
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(is));

            Scanner scanner = new Scanner(System.in);
            String line;
            while ((line = scanner.nextLine()) != null) {
                os.write((line + "\n").getBytes());
                System.out.println(reader.readLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("client done");
    }
}
