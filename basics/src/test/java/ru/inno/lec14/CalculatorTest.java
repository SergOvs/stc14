package ru.inno.lec14;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalculatorTest.class);

    private Calculator calc;

    @BeforeAll
    static void init() {
        LOGGER.info("init ALL");
    }

    @BeforeEach
    void setUp() {
        calc = new Calculator();
        LOGGER.info("setup");
    }

    @Test
    void sum() {
        LOGGER.info("sum...");
        assertEquals(7, calc.sum(3,4), "3 + 4 = 7");
        assertEquals(-1, calc.sum(3,-4), "3 + (-4) = -1");
    }

    @Test
    void divide() throws DivideByZeroException {
        LOGGER.info("devide...");
        assertEquals(2, calc.divide(4,2), "4 / 2 = 2");
        assertEquals(1, calc.divide(3,2), "3 / 2 = 1 (int)");
        assertEquals(1.5, calc.divide(3.0,2), "3 / 2 = 1 (double)");
    }

    @Test
    void divideNegotiveTest() throws DivideByZeroException {
        LOGGER.info("exception...");
        assertThrows(DivideByZeroException.class,
                () -> calc.divide(1, 0),
                "devide by zero is prohibited");
    }
}