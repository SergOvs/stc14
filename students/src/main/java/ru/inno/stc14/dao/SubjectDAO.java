package ru.inno.stc14.dao;

import ru.inno.stc14.entity.Subject;

import java.util.List;

public interface SubjectDAO {

    List<Subject> getList();

    boolean addSubject(Subject subject);
}
