package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.ConnectionManager;
import ru.inno.stc14.dao.SubjectDAO;
import ru.inno.stc14.entity.Subject;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubjectDAOImpl implements SubjectDAO {

    private final ConnectionManager manager = ConnectionManagerImpl.getInstance();

    private static final String INSERT_SUBJECT_SQL_TEMPLATE =
            "insert into subject (description) values (?)";
    private static final String SELECT_SUBJECT_SQL_TEMPLATE =
            "select id, description from subject";

    @Override
    public List<Subject> getList() {
        List<Subject> result = new ArrayList<>();
        Connection connection = manager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_SUBJECT_SQL_TEMPLATE)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Subject subject = new Subject();
                    subject.setId(resultSet.getInt(1));
                    subject.setDescription(resultSet.getString(2));
                    result.add(subject);
                }
            }

        } catch (SQLException e) {
            // TODO: 2019-02-14 Логирование исключений
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean addSubject(Subject subject) {
        Connection connection = manager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_SUBJECT_SQL_TEMPLATE)) {
            statement.setString(1, subject.getDescription());
            statement.execute();
            return true;
        } catch (SQLException e) {
            // TODO: 2019-02-14 Логирование исключений
            e.printStackTrace();
            return false;
        }
    }
}
