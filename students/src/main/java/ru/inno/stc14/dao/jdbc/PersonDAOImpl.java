package ru.inno.stc14.dao.jdbc;

import ru.inno.stc14.dao.ConnectionManager;
import ru.inno.stc14.dao.PersonDAO;
import ru.inno.stc14.entity.Person;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PersonDAOImpl implements PersonDAO {

    private final ConnectionManager manager = ConnectionManagerImpl.getInstance();

    private static final String INSERT_PERSON_SQL_TEMPLATE =
            "insert into person (name, birth_date) values (?, ?)";
    private static final String SELECT_PERSON_SQL_TEMPLATE =
            "select id, name, birth_date from person";

    @Override
    public List<Person> getList() {
        List<Person> result = new ArrayList<>();
        Connection connection = manager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SELECT_PERSON_SQL_TEMPLATE)) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Person person = new Person();
                    person.setId(resultSet.getInt(1));
                    person.setName(resultSet.getString(2));
                    Date date = new Date(resultSet.getLong(3));
                    person.setBirthDate(date);
                    result.add(person);
                }
            }

        } catch (SQLException e) {
            // TODO: 2019-02-14 Логирование исключений
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean addPerson(Person person) {
        Connection connection = manager.getConnection();
        try (PreparedStatement statement = connection.prepareStatement(INSERT_PERSON_SQL_TEMPLATE)) {
            statement.setString(1, person.getName());
            if (person.getBirthDate() == null) {
                statement.setNull(2, Types.BIGINT);
            } else {
                statement.setLong(2, person.getBirthDate().getTime());
            }
            statement.execute();
            return true;
        } catch (SQLException e) {
            // TODO: 2019-02-14 Логирование исключений
            e.printStackTrace();
            return false;
        }
    }
}
