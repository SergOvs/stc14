<%@ page import="ru.inno.stc14.entity.Person" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Студенты</title>
    <meta charset="UTF-8" />
  </head>
  <body>
  <table>
    <tr><th>Код</th><th>Имя</th><th>Дата рождения</th></tr>
    <%
      List<Person> list = (List<Person>) request.getAttribute("persons");
      for (Person person : list) {%>
    <tr><td><%=person.getId()%></td><td><%=person.getName()%></td><td><%=person.getBirthDate()%><BR>
    <%
      }
    %>
  </table>
  </body>
</html>
